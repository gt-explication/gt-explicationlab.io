---
layout: page
title: About
permalink: /about/
---

Le groupe de travail sur l'explicabilité du GDR-IA regroupe des chercheurs et industriels intéressés par les problématiques de l'explicabilité des systèmes d'I.A. 
Le GT organise ou aide à l'organisation de journées ou évènements autour de cette thématique, et plus généralement facilite le partage d'informations entre ses membres. 

Pour s'inscrire sur la liste de diffusion du GT, il suffit d'envoyer un message à: 

gt-explication-XXX@gdria.fr

avec XXX remplacé par subscribe


Les animateurs du GT sont Christophe Denis (EDF-R&D) et Nicolas Maudet (LIP6, Paris-Sorbonne Université). 


(Ce site est basé sur le basic Jekyll theme, voir [jekyllrb.com](http://jekyllrb.com/)). 
