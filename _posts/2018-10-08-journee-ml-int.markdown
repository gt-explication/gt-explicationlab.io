---
layout: post
title:  "Journée Machine Learning and Explainability"
date:   2018-10-08
categories: workshop
---

Le GT participe à l'orgnisation de la journée "Machine Learning and Explainability".  
* lundi 8 Octobre
* Hôtel Dupanloup, Orléans

Tous les détails sont disponibles sur cette [page](http://www.univ-orleans.fr/lifo/Members/vrain/MLE18/WS_MLE18.html)


