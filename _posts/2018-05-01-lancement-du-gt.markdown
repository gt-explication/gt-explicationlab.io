---
layout: post
title:  "Lancement officiel du GT!"
date:   2018-05-01 
categories: vie-du-gt
---

Le groupe de travail sur l'explicabilité est lancé par le GdR. Toute personne intéressée peut se manifester en nous envoyant un message. 
