---
layout: post
title:  "Réunion du GT [01/10/18]"
date:   2018-10-01 
categories: journees
---

La veille des assises du GdR-IA, au cours de laquelle nous le plaisir d'écouter en particulier Tarek Besold,  nous organisions sur une matinée une réunion du GT explicabilité, avec quatre présentations. 
* lundi 1er Octobre
* LIP6, Sorbonne Université, Campus Pierre et Marie Curie, Paris
* salle 25-26/105



# Programme

* 09:00: Accueil

* 09:30: Thibault Laugel (LIP6): Defining Locality for Surrogates in Post-hoc Interpretablity

* 10:00: Ismail Baaj (CEA): Une approche pour la génération d’explication dans les systèmes experts flous

* 10:30: Pause-café 

* 11:00: Christophe Denis (EDF R&D, LIP6), Jean-Gabriel Ganascia (LIP6) - Vers la validation et  l’explicabilité de  l’IA Statistique

* 11:30: Noélie Cherrier (CEA) - Construction de features interprétables pour la classification en physique expérimentale

Accès aux [documents](https://gitlab.com/gt-explication/journee-2018-10-01/)
