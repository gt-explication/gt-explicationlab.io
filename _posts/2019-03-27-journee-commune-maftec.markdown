---
layout: post
title:  "Journée commune des GT MAFTEC et explicabilité"
date:   2019-03-27
categories: workshop
---


Les 6èmes journées du groupe de travail du GdR IA sur la planification Multi-Agent, Flexible, Temporelle, Épistémique et Contingente (MAFTEC, https://www.gdria.fr/gt-maftec/) auront lieu à Caen du 1er au 3 avril 2019 (campus 2 Côte de Nacre, bâtiment Sciences 3).
Le lundi 1er avril sera consacré à une journée commune des groupes de travail MAFTEC et Explicabilité (https://www.gdria.fr/gt-explicabilite/) du GdR IA, avec un accent sur l'explication de plans et la planification d'explications.

Un programme prévisionnel est disponible à l'URL : https://www.irit.fr/~Frederic.Maris/maftec6/programme.php.

Ces journées cherchent à favoriser les échanges. Elles seront composées d'exposés et de sessions de travail ; les salles permettront le travail en grand groupe et en plus petits groupes à la fois. Les propositions d'exposés sont les bienvenues : sur des résultats, des pistes de recherche, des applications, etc. Vous pouvez également proposer des thèmes de recherche ou des invités que vous aimeriez voir présenter lors des journées. Pour tout cela, il suffit d'envoyer quelques lignes explicatives à frederic.maris@irit.fr, andreas.herzig@irit.fr (pour le GT MAFTEC), bruno.zanuttini@unicaen.fr (organisateur des journées), nicolas.maudet@lip6.fr (pour le GT Explicabilité). Enfin, des temps de type « rump session » permettront aux participants de présenter un résultat, une question, etc. de façon improvisée.

La participation aux journées est gratuite et ouverte à tous ; les repas du midi sont pris en charge par les journées. Pour nous permettre de les organiser dans les meilleures conditions, merci de vous inscrire avant le 27 mars 2018 (en trois clics, en suivant le lien : https://www.irit.fr/~Frederic.Maris/maftec6/inscriptions.php).

Pour toute question pratique ou autre, ne pas hésiter à contacter Bruno Zanuttini, bruno.zanuttini@unicaen.fr, responsable de l'organisation. 

