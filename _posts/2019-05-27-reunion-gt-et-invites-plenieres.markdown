---
layout: post
title:  "Explicabilité aux journées plénières du GdR-IA à Orléans"
date:   2019-05-27
categories: workshop
---


Plusieurs évènements en lien avec le GT explicabilité dans le cadre des journées plénières du GdR-IA qui se tiendront à Orléans le 28 et 29 Mai. 

# Réunion de travail du GT 

Le lundi 27 Mai après-midi: 

* 16h - Explicabilité et convivialité d'outils de diagnostic médical basé sur de l'IA - Christophe Denis et Judith Nicogossian 
* 16h30 - Présentation de l'appel CHIST-ERA ERA-NET 2019, dont l'une des thématiques sera "Explainable Machine Learning-based Artificial Intelligence" - Mathieu Girerd (Coordinateur de l'appel)
* 17h - Session ouverte et discussion

# Invités et table ronde lors des journées plénières

Le mardi 28 Mai, dans le cadre des journées plénières: 

* 14h - Interprétation d'images médicales: quelques travaux vers l'explicabilité - Céline Hudelot (MICS, Centrale Supelec)
* 14h45 - Apprentissage artificiel et interprétabilité pour la médecine de précision - Jean-Daniel Zucker (UMMISCO, IRD, Sorbonne Université)
* 16h - Table ronde: IA et santé: quelle explicabilité? Christophe Denis, Céline Hudelot, Cloderic Mars, Nicolas Maudet, Judith Nicogossian, Jean-Daniel Zucker



