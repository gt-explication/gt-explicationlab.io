---
layout: post
title:  "Réunion Explicabilité aux journées plénières du GdR-IA à Arras"
date:   2022-09-11
categories: workshop
---


Session du GT explicabilité dans le cadre des journées plénières du GdR-IA qui se tiendront à Arras le 11 Octobre:

# Réunion de travail du GT (en construction):

Le Mardi 11 Octobre après-midi:

* 14h - Yann Munro (LIP6, Sorbonne Université) -- Argumentation and Causal Models in Human-Machine Interaction: A Round Trip
* 14h40 - Léo Saulières (IRIT, Univ. de Toulouse) -- Des explications locales de politiques à l'aide de Scénario-eXplications (SXp)
* 15h20 - Manuel Amoussou (MICS, CentraleSupelec) -- Explication de recommandations issues d'un modèle additif: de la conceptualisation à l'évaluation
* 16h - Pause café
* 16h20 - Pierre Marquis (CRIL, Univ. d'Artois) -- On the intelligibility of decision trees

# Résumé des interventions


### Yann Munro (LIP6) -- Argumentation and Causal Models in Human-Machine Interaction: A Round Trip
> To discriminate among the multitude of methods in XAI, it is interesting and enlightening to consider the problem from a social science perspective, in order to establish some principles that XAI methods should follow. To this end, the structural causal model approach and the definition proposed by J.Halpern and completed by T.Miller, adding in particular contrastivity, are rooted in works in social sciences. However, obtaining a structural causal model matching the reality can be difficult, for instance in a context of human-machine interaction. By contrast, P.M.Dung's abstract argumentation frameworks excel in this exercise.
In this presentation, I will consider a restriction of these causal models and then propose a transformation to go from one framework to the other and vice versa in order to exploit at best the good properties of each of these frameworks. This transformation also allows proving an equivalence between the two restricted models, from which their definitions of the notion of explanation can be compared.

### Léo Saulières (IRIT) -- Des explications locales de politiques à l'aide de Scénario-eXplications (SXp)
> Dans le contexte de l'apprentissage par renforcement (RL), afin d'accroître la confiance dans la politique d'un agent ou de comprendre ses défaillances dans un environnement stochastique, nous proposons des explications prédictives sous la forme de trois scénarios (séquences d'états-actions): le meilleur cas, le pire cas et le plus probable. Ces explications permettent de répondre à la question: "Qu'est ce qu'il est possible de se produire à partir d'un état, en suivant la politique de l'agent?". Trouver de tels scénarios se révéle être un problème W[1]-hard, c'est pourquoi nous fournissons des approximations de ces scénarios en temps linéaire. La particularité des approximations du meilleur, pire cas est l'utilisation du RL pour obtenir des politiques de l'environnement, vu comme un agent hostile/favorable.

### Manuel Amousso (MICS) -- Explication de recommandations issues d'un modèle additif: de la conceptualisation à l'évaluation
> Nous nous situons dans un cadre d'aide à la décision multicritère où une recommandation de type "choix de la meilleure alternative" est à expliquer : explication provenant d'un analyste humain ou artificiel et à destination d'un décideur humain dont les préférences sont représentables par un modèle additif.
L'explication porte sur l'ensemble des comparaisons entre paires d'alternatives qui légitiment le choix de la meilleure alternative et qui, de part leur complexité (au sens cognitif), peuvent susciter une interrogation de la part du décideur.
Nous proposons donc une explication qui consistera à décomposer l'ensemble des comparaisons d'alternatives à expliquer en des fragments suffisamment cognitivement simples pour être compris du décideur, fragments que nous regroupons en différentes catégories appelées langages.
Le calcul est effectué en recourant à certains outils empruntés à la Recherche Opérationnelle et quelques pistes de propositions sont apportées pour remédier le cas échéant aux situations où la comparaison à expliquer n'est pas décomposable dans le langage retenu.

### Pierre Marquis (CRIL) -- On the intelligibility of decision trees
> I will present a couple of results obtained recently in the setting of the EXPEKCTATION project.
EXPEKCTATION is the name of a research and teaching chair in AI (ANR-19-CHIA-0005-01), that is
about formal XAI. In the talk, I will focus on a well-known ML model, decision trees.
I will explain to which extent decision trees can be considered as intelligible.
