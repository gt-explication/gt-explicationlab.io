---
layout: post
title:  "Assises du GdR"
date:   2018-07-01 
categories: vie-du-gt
---

Les assises du GdR se tiendront le 2 et 3 Octobre à Paris-Dauphine. 
Ce sera pour le GT l'occasion de faire un [petit bilan](https://gitlab.com/gt-explication/journee-2018-10-01/blob/master/gt-explicabilite-bilan.pdf) des actions passées et à venir, et d'écouter [Tarek Besold](https://sites.google.com/site/tarekbesold/) nous présenter ["Symbols, Networks, Explanations: A complicated Ménage à Trois"](https://drive.google.com/file/d/1IU9tvIBa28HJu1Bw6O44nhjkO-0-QlUZ/view).  
