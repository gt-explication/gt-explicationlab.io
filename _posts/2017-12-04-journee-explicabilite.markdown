---
layout: post
title:  "Journée explicabilité des systèmes d'I.A."
date:   2017-12-04 
categories: journees
---

Un [droit à l’explication](https://en.wikipedia.org/wiki/Right_to_explanation)
pour les décisions algorithmiques est mis
en avant par la loi pour une République Numérique, ainsi que par le
règlement général sur la protection des données (General Data
Protection Regulation, GDPR) --qui entre en vigueur pour les membres
de l’Union Européenne en Mai 2018--. Même si des questions juridiques
restent ouvertes quant à la portée et aux conséquences de ces textes,
cette notion constitue un défi important, en particulier pour les
systèmes d’I.A.:
De fait, cette question est depuis longtemps centrale dans le domaine
(elle est présente dès les premiers travaux autour des systèmes
experts en particulier), car cette capacité à expliquer peut
s’avérer cruciale dans de nombreuses applications.

Le [pré-GdrIA](http://www.gdria.fr/) organise une journée de travail
sur l’explicabilité des systèmes d’I.A., avec pour objectif de
discuter (liste non exhaustive):
*de la portée de la règlementation, du sens exact à donner à
l’explicabilité dans ce cadre (quelle différence avec la notion
d'interprétabilité? de redevabilité?);
* de l’état actuel des recherches: explicabilité en aide à la
décision, en apprentissage; explicabilité des résultats
vs. des modèles (symboliques/numériques);
* des initiatives internationales autour de cette question
(eg. Explainable AI).


La journée vise à réunir académiques et industriels afin d'établir un
état des lieux, en vue de créer éventuellement un groupe de travail
sur le sujet dans le cadre du pré-GdrIA.


# Programme

Matin:

* 09:30: Accueil

* 09:50: Nicolas Maudet (LIP6):
Présentation de la journée

* 10:15: Christophe Labreuche (Thales):
Explaining decisions based on weighted sums, and a little bit more

* 11:00: Freddy Lecue (INRIA/Accenture Tech. Labs):
On Delivering AI to People - From Machine Learning to Explainable AI

* 11:45: Yann Chevaleyre (LAMSADE, Paris-Dauphine):
An overview of interpretability issues in Machine Learning

Après-midi:

* 14:00--16:00: Contributions

* Olivier Cailloux (LAMSADE, Paris-Dauphine):
A formal framework for deliberated judgment

* Jean-Louis Dessalles (Telecom ParisTech):
Le prédicat et le paradoxe de l'explicabilité: comment interfacer des
techniques numériques et symboliques?

* Régis Pierrard (CEA):
Apprentissage de relations spatiales pour l'annotation sémantique
justifiée

* Sylvie Doutre (IRIT, Univ. Toulouse):
Explicabilité pour l’argumentation

* Khaled Belahcene (LGI, CentraleSupelec):
Explication de décisions robustes

* Christian de Sainte-Marie (IBM):
Explanation Issues at IBM

* 16:00: Discussion

Accès aux [documents](https://gitlab.com/gt-explication/journee-2017-21-04/blob/master/README.md)
